macro(PARTICLETABLE_COLLATE_APPLICATION_SOURCES source_dest_var)
  file(GLOB 
    ${source_dest_var} 
    ${CMAKE_CURRENT_BINARY_DIR}/particletable.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/src/*.cc
    )
endmacro()