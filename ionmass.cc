/*
ParticleTable Copyright (C) Royal Holloway, 
University of London 2001 - 2017.

This file is part of ParticleTable.

ParticleTable is free software: you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published 
by the Free Software Foundation version 3 of the License.

ParticleTable is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ParticleTable.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ParticleTable.hh"

#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <vector>

int main(int argc, char* argv[])
{  
  if (argc < 3)
    {
      std::cout << "Too few arguments!" << std::endl;
      std::cout << "ionmass Z A (l) (lvl)" << std::endl;
      return 0;
    }

  std::vector<int> values = {0,0,0,0}; // z,a,l,lvl
  for (int i = 0; i < argc-1; i++)
    {values[i] = std::atoi(argv[i+1]);}

  PT::ParticleTable tb;
  double nucleusMass = tb.NucleusMass(values[0],
				      values[1],
				      values[2],
				      values[3]);
  
  int pdgid = tb.NucleusEncoding(values[0],
				 values[1],
				 values[2],
				 values[3]);
  double brho = tb.MagneticRigidity(pdgid, 1.2e6);
  
  std::cout << std::setprecision(14) << std::scientific << nucleusMass << " MeV" << std::endl;
  std::cout << "PDG ID: "            << pdgid << std::endl;
  std::cout << "Magnetic Rigidity: " << brho  << std::endl;
  
  return 0;
}
