/*
ParticleTable Copyright (C) Royal Holloway, 
University of London 2001 - 2017.

This file is part of ParticleTable.

ParticleTable is free software: you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published 
by the Free Software Foundation version 3 of the License.

ParticleTable is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ParticleTable.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ParticleTable.hh"

#include "G4IonTable.hh"
#include "G4ParticleTable.hh"
#include "G4Types.hh"

#include "TDatabasePDG.h"
#include "TParticlePDG.h"

#include <cmath>
#include <limits>
#include <string>

PT::ParticleTable::ParticleTable()
{
  particleTable = G4ParticleTable::GetParticleTable();
  ionTable      = particleTable->GetIonTable();
  rootPDGTable  = new TDatabasePDG();
}

PT::ParticleTable::~ParticleTable()
{;}

double PT::ParticleTable::ParticleMass(int  pdgEncoding,
				       bool assumeFullyIonised) const
{
  if (IsAnIon(pdgEncoding))
    {// it's an ion
      G4int z,a,l,lvl;
      ParseIonPDG(pdgEncoding, z,a,l,lvl);
      if (assumeFullyIonised)
	{return NucleusMass(z,a,l,lvl);}
      else
	{return IonMass(z,a,l,lvl);}
    }
  else
    {
      TParticlePDG* particle = rootPDGTable->GetParticle(pdgEncoding);
      if (!particle)
	{return 0;}
      else
	{return particle->Mass();}
    }
}

double PT::ParticleTable::ParticleCharge(int pdgEncoding) const
{
  if (IsAnIon(pdgEncoding))
    {// it's an ion
      G4int z,a,l,lvl;
      ParseIonPDG(pdgEncoding, z,a,l,lvl);
      return (double)z;
    }
  else
    {
      TParticlePDG* particle = rootPDGTable->GetParticle(pdgEncoding);
      if (!particle)
	{return 0;}
      else
	{
	  double charge = particle->Charge();
	  // root gives charge at 3x normal (of course!)
	  if (std::abs(charge) > std::numeric_limits<double>::epsilon())
	    {charge /= 3.0;}
	  return charge;
	}
    }
}

double PT::ParticleTable::NucleusMass(const int& Z,
				      const int& A,
				      int L,
				      int lvl) const
{
  G4double resultG4 = ionTable->GetNucleusMass(Z, A, L, lvl);
  return static_cast<double>(resultG4);
}

double PT::ParticleTable::IonMass(const int& Z,
				  const int& A,
				  int L,
				  int lvl) const
{
  G4double resultG4 = ionTable->GetIonMass(Z, A, L, lvl);
  return static_cast<double>(resultG4);
}

int PT::ParticleTable::NucleusEncoding(const int& Z,
				       const int& A,
				       double     energy,
				       int        lvl) const
{
  G4int resultG4 = G4IonTable::GetNucleusEncoding(Z,A,energy,lvl);
  return static_cast<int>(resultG4);
}
 
std::string PT::ParticleTable::ParticleName(int pdgEncoding) const
{
  if (IsAnIon(pdgEncoding))
    {
      int Z, A, L, lvl;
      ParseIonPDG(pdgEncoding, Z, A, L, lvl);
      G4String resultG4 = ionTable->GetIonName(Z, A, L, lvl);
      return static_cast<std::string>(resultG4);
    }
  else
    {
      TParticlePDG* particle = rootPDGTable->GetParticle(pdgEncoding);
      if (!particle)
	{return "unknown";}
      else
	{return std::string(particle->GetName());}
    }
}
   
bool PT::ParticleTable::IsAnIon(int pdgEncoding) const
{
  return pdgEncoding > 10000000;
}

void PT::ParticleTable::ParseIonPDG(const int& pdgEncoding,
				    int& Z,
				    int& A,
				    int& L,
				    int& lvl) const
{
  G4double temporaryEnergy = 0; // needed for function call
  G4bool resultG4 = ionTable->GetNucleusByEncoding(pdgEncoding,
						   Z, A, L, temporaryEnergy, lvl);

  if (!resultG4)
    {std::cout << "Could not find ion with PDG code \"" << pdgEncoding << "\"" << std::endl;}
}

double PT::ParticleTable::MagneticRigidity(const int&    pdgEncoding,
					   const double& totalEnergy) const
{
  double mass       = ParticleMass(pdgEncoding);
  if (totalEnergy < mass) // check
    {
      std::cout << "Total Energy " << totalEnergy << " is less than rest mass " << mass << " MeV" << std::endl;
      return 0;
    }
  int    charge     = ParticleCharge(pdgEncoding);
  if (!(std::abs(charge) > std::numeric_limits<double>::epsilon())) // not finite, ie 0
    {return 0.0;} // no charge -> 0 rigidity
  
  double numerator   = std::sqrt(std::pow(totalEnergy,2) - std::pow(mass,2)) * 1e6;// sqrt(E_t*2 - E_rest*2) in eV
  double denominator = charge * 2.99792458e8; // q c
  double brho        = numerator / denominator;
  return brho;
}
