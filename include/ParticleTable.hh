/*
ParticleTable Copyright (C) Royal Holloway, 
University of London 2001 - 2017.

This file is part of ParticleTable.

ParticleTable is free software: you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published 
by the Free Software Foundation version 3 of the License.

ParticleTable is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ParticleTable.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PMPARTICLETABLE_H
#define PMPARTICLETABLE_H

#include <string>

#include "TObject.h"

class G4IonTable;
class G4ParticleTable;

class TDatabasePDG;

namespace PT
{
  /**
   * @brief Holder for Geant4 particle table.
   *
   * @author Laurie Nevay
   */
  
  class ParticleTable: public TObject
{
public:
  ParticleTable();
  ~ParticleTable();

  /// Get the mass of a particle. If it is an ion, it will be assumed fully ionised
  /// and the mass of nucleus is returned.
  double ParticleMass(int pdgEncoding,
		      bool assumeFullyIonised = true) const;

  /// Get the particle charge. Only works for fully ionised ions.
  double ParticleCharge(int pdgEncoding) const;

  /// Mass of the fully ionised nucleus.
  double NucleusMass(const int& Z,
		     const int& A,
		     int        L   = 0,
		     int        lvl = 0) const;

  /// Mass of the ion.
  double IonMass(const int& Z,
		 const int& A,
		 int        L   = 0,
		 int        lvl = 0) const;

  /// For a given set of parameters, prepare the PDG encoding for an ion.
  int NucleusEncoding(const int& Z,
		      const int& A,
		      double     energy = 0,
		      int        lvl    = 0) const;

  /// Get the particle name from PDG ID.
  std::string ParticleName(int pdgEncoding) const;

  /// Wether the particle is an ion or not from the PDG ID.
  bool IsAnIon(int pdgEncoding) const;

  /// Interpret the PDG ID for an ion to get the content of the nucleus.
  void ParseIonPDG(const int& pdgEnconding,
		   int& Z,
		   int& A,
		   int& L,
		   int& lvl) const;

  /// Return the magnetic rigidity of a particle given a total energy. The total
  /// energy must be greater than the rest mass of the particle. Neutral particles
  /// have a rigidity of 0.
  double MagneticRigidity(const int&    pdgEncoding,
			  const double& totalEnergy) const;
  
private:
  G4ParticleTable* particleTable; ///< Reference to partilce table. Does not own.
  G4IonTable*      ionTable;      ///< Reference to the ion table. Does not own.
  TDatabasePDG*    rootPDGTable;  ///< ROOT particle table.
  
  ClassDef(ParticleTable,1);
};
}

#endif
