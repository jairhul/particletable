/*
ParticleTable Copyright (C) Royal Holloway, 
University of London 2001 - 2017.

This file is part of ParticleTable.

ParticleTable is free software: you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published 
by the Free Software Foundation version 3 of the License.

ParticleTable is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ParticleTable.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma link C++ class PT::ParticleTable+;
#pragma link C++ namespace PT;
