import ROOT

from ROOT import gSystem


gSystem.Load("libparticletable")

import ROOT.PT
from ROOT.PT import ParticleTable

a = ParticleTable()

mass1 = a.NucleusMass(54,151)
pdg1  = a.NucleusEncoding(54,151)
name1 = a.ParticleName(pdg1)

print "Particle   ",name1
print "PDG ID     ",pdg1
print "Mass (MeV) ",mass1
